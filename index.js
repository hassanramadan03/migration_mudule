const MongoClient = require("mongodb");
require("dotenv").config({ silent: true });
const url = process.env.URL;

MongoClient.connect(url, { useNewUrlParser: true }, async function(  err, client ) {
  try {
    console.log(err)
    // ::::::::::::::-------------------------------------- ::::::::::::::::
    // ::::======>>>>     Define your DBS & collections      <<<<========:::
    // ::::::::::::::_______________________________________::::::::::::::::
    const doer={db_name:'merchantsDB',collection:'merchants'}
    const databases=[
      {db_name:'paymentPagesDB',collection:'paymentPages'},
      {db_name:'SubscriptionServiceDB',collection:'plans'},
      {db_name:'paymentRequestDB',collection:'paymentRequests'},
    ]
  

    const migratedScript = await migrate(client,doer, databases);
    console.log(migratedScript)
   
  } catch (error) {
    console.log(error);
  }
});

// ::::::::::::::---------------------------------------::::::::::::::::
// ::::======>>>>         WRITE YOUR LOGIC HERE         <<<<========::::
// ::::::::::::::_______________________________________::::::::::::::::

const migrate = async (client,doer, databases) => {
  return new Promise(async (resolve, reject) => {
    try {
       
       const merchant_collection=client.db(doer.db_name).collection(doer.collection)
       await merchant_collection.find({}).forEach(async(merchant)=>{
            databases.forEach(_db=>{
              let collection=client.db(_db.db_name).collection(_db.collection);
              collection.update({merchantId:merchant.merchantPayformanceId},{$set:{creationById:merchant.createdUserId}})
            })
          })
     
       resolve({ success: true });
    } catch (error) {
      reject({ success: false });
    }
  });
};
